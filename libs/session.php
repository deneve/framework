<?php
/*
 * Библиотека для работы с сессиями
 */
class Session extends \Core\Lib
{
    private static $_instance = null;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        self::$_instance->_start();

        return self::$_instance;
    }

    private function _start()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function close()
    {
        unset($_SESSION);
    }

    public function __get($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : false;
    }

    public function __set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public function get($name = false)
    {
        if ($name) {
            return isset($_SESSION[$name]) ? $_SESSION[$name] : false;
        } else {
            return $_SESSION;
        }
    }

    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public function delete($name)
    {
        return $this->remove($name);
    }

    public function remove($name)
    {
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
            return true;
        } else {
            return false;
        }
    }
}