<?php
/*
 * Библиотека для работы с аутентификацией пользователей
 */
class Auth extends \Core\Lib
{
    private static $_instance = null;

    private $hashMethod, $solt, $token, $tokenLife;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        self::$_instance->hashMethod = appDeneve()->config('components.auth.hashMethod');
        self::$_instance->solt = appDeneve()->config('components.auth.solt');
        self::$_instance->token = appDeneve()->config('components.auth.token');
        self::$_instance->tokenLife = appDeneve()->config('components.auth.tokenLife');

        return self::$_instance;
    }

    /**
     * Проверяет, авторизован ли пользователь
     */
    public function is()
    {
        $auth = false;
        if ($this->app()->session->authUser) {
            $auth = true;
        } else {
            /*if ($this->app->cookie->authToken) {

            }*/
        }

        return $auth;
    }

    /**
     * Авторизация пользователя
     * @param object $model
     * @param string $fieldsSearch через запятую (поля поиска пользователя)
     * @param string $fieldsCondition через запятую (поля сравнения)
     * @return bool
     */
    public function login($model, $fieldsSearch, $fieldsCondition)
    {
        $return = false;
        $whereFields = ''; $whereValues = [];
        $authConfig = $this->app()->config('components.auth');
        $authConfigFields = explode(',', $authConfig['hash']['fields']);
        $authConfigFields = array_map(function($elem) {return trim($elem);}, $authConfigFields);
        $fieldsSearch = array_map(function($elem) {return trim($elem);}, explode(',', $fieldsSearch));
        $fieldsCondition = array_map(function($elem) {return trim($elem);}, explode(',', $fieldsCondition));
        foreach ($fieldsSearch as $field) {
            if (isset($model->getAttributes()[$field]) && !empty($value = $model->getAttributes()[$field])) {
                $whereFields.= 'AND ' . $field . ' = ? ';
                $whereValues[] =  $value;
            } else {
                break;
            }
        }

        $ok = 0;
        if (count($whereValues) && count($whereValues) == count($fieldsSearch)) {
            $whereFields = substr($whereFields, 3);
            if ($user = $model->findOne($whereFields, $whereValues)) {
                foreach ($fieldsCondition as $field) {
                    $attributes = $model->getAttributes();
                    $value = isset($attributes[$field]) ? $attributes[$field] : '';
                    if ($value && in_array($field, $authConfigFields)) {
                        switch ($authConfig['hash']['method']) {
                            case 'md5':
                                $value = md5($value . (!is_null($authConfig['hash']['solt']) ? $authConfig['hash']['solt'] : ''));
                                break;
                            case 'sha1':
                                $value = sha1($value . (!is_null($authConfig['hash']['solt']) ? $authConfig['hash']['solt'] : ''));
                        }
                    }
                    if (!empty($value) && $user->{$field} == $value) {
                        $ok++;
                    } else {
                        $model->validateErrors[$field] = 'error';
                    }
                }
            } else {
                $model->validateErrors['_users'] = 'no found';
            }
        }

        if ($ok && $ok == count($fieldsCondition)) {
            $this->app()->session->authUser = true;
            $return = true;
        }
        return $return;
    }

    /**
     * Сбросить авторизацию
     */
    public function logout()
    {
        return $this->app()->session->remove('authUser');
    }

}