<?php
/*
 * Библиотека для работы со сторонними компонентами
 * некоторая часть хелперов нуждается в Jquery библиотеке, подключенной в хедер
 */
class Helper extends \Core\Lib
{
    private static $_instance = null;

    private $addActiveData, $formData;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Объекты модели конвертирует в массив с нужными полями
     * @param Model $models
     * @param string|array $fields
     * @param string|bool $key
     * @return array
     */
    public function listData($models, $fields, $key = false)
    {
        $fieldArr = is_array($fields);
        $data = [];
        foreach ($models as $model) {
            if ($fieldArr) {
                $value = [];
                foreach ($fields as $field) {
                    $value[$field] = $model->{$field};
                }
            } else {
                $value = $model->{$fields};
            }
            if ($key) {
                $data[$model->{$key}] = $value;
            } else {
                $data[] = $value;
            }
        }
        return $data;
    }

    /**
     * @param string $string
     * @param string $pre
     * @return string
     */
    public function stringToUrl($string, $pre = '_')
    {
        return preg_replace("/[^а-яёa-z" . $pre . "]/u", '', mb_strtolower($string, 'utf-8'));
    }

    /**
     * Добавляет "active" класс к элементу или родителю от href в зависимости от текущей страницы
     * @param string $className
     * @param array $params
     */
    public function addActiveBegin($className = 'active', $params = [])
    {
        $this->addActiveData = [
            'className' => $className,
            'params' => $params
        ];
        ob_start();
    }

    public function addActiveEnd()
    {
        $block = ob_get_contents();
        ob_end_clean();
        $doc = $this->app()->dom->load($block);
        foreach ($doc->findByTag('a') as $elem) {
            if ($elem->hasAttribute('href') && !empty($href = $elem->getAttribute('href'))) {
                $r = $this->app()->getRoute($href);
                if (
                    $this->app()->get('module') == $r['module'] &&
                    $this->app()->get('controller') == $r['controller'] &&
                    $this->app()->get('action') == $r['action'] &&
                    (!isset($this->addActiveData['params']['checkParams']) || $this->app()->get('params') == $r['params'])
                ) {
                    if (isset($this->addActiveData['params']['parent'])) {
                        $elem->parentNode->setAttribute('class', $this->addActiveData['className']);
                    } else {
                        $elem->setAttribute('class', $this->addActiveData['className']);
                    }
                    break;
                }
            }
        }
        echo $doc->save();
    }

    public function breadcrumbs()
    {

    }

    public function formBegin($model, $params = [])
    {
        $this->formData = [
            'model' => $model,
            'params' => $params
        ];
        ob_start();
    }

    public function formEnd()
    {
        $block = ob_get_contents();
        ob_end_clean();

        if (is_object($this->formData['model']) && is_array($this->formData['model']->getAttributes())) {
            $doc = $this->app()->dom->load($block);
            foreach ($doc->findByTag('input') as $elem) {
                if ($elem->hasAttribute('name')) {
                    $name = $elem->getAttribute('name');
                    if (!empty($name) && isset($this->formData['model']->getAttributes()[$name])) {
                        $value = $this->formData['model']->getAttributes()[$name];
                        $elem->setAttribute('value', $value);
                        if (isset($this->formData['model']->validateErrors[$name])) {
                            $err = $doc->findById($name . 'Error');

                            $err->appendChild($doc->getDom()->createTextNode($this->formData['model']->validateErrors[$name]));

                            //$dom->getElementById($name . 'Error')->appendChild($dom->createTextNode($this->formData['model']->validateErrors[$name]));
                        }
                    }
                }
            }
        }

        /*if (is_object($this->formData['model']) && is_array($this->formData['model']->getAttributes())) {
            $dom = new DOMDocument();
            if ($dom->LoadHtml(mb_convert_encoding($block, 'HTML-ENTITIES', 'UTF-8'))) {
                foreach($dom->getElementsByTagName('input') as $input) {
                    $name = $input->getAttribute('name');
                    if (!empty($name) && isset($this->formData['model']->getAttributes()[$name])) {
                        $value = $this->formData['model']->getAttributes()[$name];
                        $input->setAttribute('value', $this->formData['strip_tags'] ? strip_tags($value) : $value);
                        if (isset($this->formData['model']->validateErrors[$name])) {
                            $dom->getElementById($name . 'Error')->appendChild($dom->createTextNode($this->formData['model']->validateErrors[$name]));
                        }
                    }
                }
                $block = $dom->saveHTML($dom->documentElement);
                $block = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $block);
            }
        }*/
        $params = '';
        foreach ((array)$this->formData['params'] as $key => $param) {
            $params.= ' ' . $key . '="' . $param . '"';
        }
        echo '<form'.$params.'>' . $block . '</form>';
    }
}