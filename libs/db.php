<?php

class Db extends \Core\Lib
{
    private static $_instance = null;

    const RETURN_ONE = 1;
    const RETURN_ALL = 2;
    const RETURN_COUNT = 3;

    private $db = null;
    private $table = null;

    static public function getInstance($arguments)
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        self::$_instance->connect(
            isset($arguments[0]) ? $arguments[0] : '',
            isset($arguments[1]) ? $arguments[1] : appDeneve()->config('components.db.type')
        );

        return self::$_instance;
    }

    private function connect($table, $type)
    {
        if (is_null($this->table) || $this->table != $table) {
            $this->table = $table;
        }
        if (is_null($this->db)) {
            if ($type == 'mysql') {
                try {
                    $this->db = new PDO(
                        $this->app()->config('components.db.dsn'),
                        $this->app()->config('components.db.username'),
                        $this->app()->config('components.db.password'),
                        [PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true]
                    );
                } catch (PDOException $e) {
                    die ('Connection MySQL failed: ' . $e->getMessage());
                }
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db->exec('SET NAMES `' . $this->app()->config('components.db.charset') . '`');
            }
        }
    }

    private function prepare($prepare, $values, $params, $return)
    {
        if ($return == self::RETURN_COUNT) {
            $select = 'COUNT(*) as count';
        } else {
            $select = isset($params['select']) ? (string)$params['select'] : '*';
        }
        if (is_numeric($prepare) && !count($values)) {
            $values[] = $prepare;
            $prepare = 't.id = ?';
        }
        $prepare = !empty($prepare) ? 'WHERE ' . $prepare : '';
        if (!is_array($values)) {
            $values = [$values];
        }
        $join = isset($params['join']) ? (string)$params['join'] : '';
        $group = isset($params['group']) ? 'GROUP BY ' . (string)$params['group'] : '';
        $having = isset($params['having']) ? 'HAVING ' . (string)$params['having'] : '';
        $order = isset($params['order']) ? 'ORDER BY ' . (string)$params['order'] : '';
        $limit = isset($params['limit']) ? 'LIMIT ' . (string)$params['limit'] : '';
        $procedure = isset($params['procedure']) ? 'PROCEDURE ' . (string)$params['procedure'] : '';
        try {
            $stmt = $this->db->prepare("SELECT {$select} FROM {$this->table} as t {$join} {$prepare} {$group} {$having} {$order} {$limit} {$procedure}");
            $stmt->execute($values);
            if ($return == self::RETURN_ONE) {
                if ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    return $result;
                } else {
                    return false;
                }
            } elseif ($return == self::RETURN_ALL) {
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $returs = [];
                foreach ($results as $result) {
                    $returs[] = $result;
                }
                return $returs;

            } elseif ($return == self::RETURN_COUNT) {
                return (int)$stmt->fetch(PDO::FETCH_ASSOC)['count'];
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Выполняет select запрос с limit = 1
     * @return object dbRow
     */
    public function findOne($prepare = '', $values = [], $params = null)
    {
        return $this->prepare($prepare, $values, $params, self::RETURN_ONE);
    }

    /**
     * Выполняет select запрос
     * @return array object's dbRow
     */
    public function find($prepare = '', $values = [], $params = null)
    {
        return $this->prepare($prepare, $values, $params, self::RETURN_ALL);
    }

    /**
     * Возвращает кол-во записей
     * @return int
     * @throws Exception
     */
    public function count($prepare = '', $values = [], $params = null)
    {
        return $this->prepare($prepare, $values, $params, self::RETURN_COUNT);
    }

    /**
     * Обновляет запись в таблице
     * @return int / количество строк, модифицированных последним SQL запросом
     * @throws Exception
     */
    public function update($where = '', $values = [], $updates = [],  $params = null)
    {
        $updatesEx = '';
        $valuesEx = [];

        if ($where) {
            if (is_array($where)) {
                $updates = $where;
                $values = [];
                $where = '';
            } else {
                if (is_numeric($where)) {
                    $updatesEx = (string)$values;
                    $valuesEx = $updates;
                    $valuesEx[] = $where;
                    $where = 'id = ?';
                }
                if (!is_array($values)) {
                    $values = [$values];
                }
                $where = 'WHERE ' . $where;
            }
        }

        if (!$updatesEx) {
            foreach ($updates as $key => $update) {
                $updatesEx .= ', ' . $key . ' = ?';
                $valuesEx[] = $update;
            }
            if (!empty($updatesEx)) {
                $updatesEx = ltrim($updatesEx, ',');
            }

            foreach ($values as $value) {
                $valuesEx[] = $value;
            }
        }

        try {
            $stmt = $this->db->prepare("UPDATE {$this->table} SET {$updatesEx} {$where}");
            $stmt->execute($valuesEx);
            return $stmt->rowCount();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Удаляет запись из таблицы
     * @return int / количество строк, модифицированных последним SQL запросом
     * @throws Exception
     */
    public function delete($where = '', $values = [], $params = null)
    {
        // TODO: удалить всех, пример из update взять
        if (is_numeric($where)) {
            $values = [$where];
            $where = 'id = ?';
        } else {
            if (!is_array($values)) {
                $values = [$values];
            }
        }
        if (!empty($where)) {
            $where = 'WHERE ' . $where;
        }
        $order = isset($params['order']) ? 'ORDER BY ' . $params['order'] : '';
        $limit = isset($params['limit']) ? 'LIMIT ' . $params['limit'] : '';

        try {
            $stmt = $this->db->prepare("DELETE FROM {$this->table} {$where} {$order} {$limit}");
            $stmt->execute($values);
            return $stmt->rowCount();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function query($sql)
    {
        return $this->db->query($sql);
    }

    // TODO: транзакции
    // TODO: insert, truncate, createTable, dropTable, addColumns, dropColumns, addForeignKeys, dropForeignKeys
}