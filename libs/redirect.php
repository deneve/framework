<?php
/*
 * Библиотека для работы с перенаправлениями
 */
class Redirect extends \Core\Lib
{
    private static $_instance = null;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        if (isset($arguments[0])) {
            self::$_instance->go(
                $arguments[0],
                isset($arguments[1]) ? (int)$arguments[1] : 302
            );
        }

        return self::$_instance;
    }

    private function go($url, $statusCode)
    {
        header('Location: ' . $url, false, $statusCode);
        exit;
    }

    public function refresh()
    {

    }
}