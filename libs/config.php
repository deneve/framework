<?php
/*
 * Библиотека для работы с конфигом приложения
 */
class Config extends \Core\Lib
{
    private static $_instance = null;

    private $configDefault = null;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        $get = isset($arguments[0]) ? $arguments[0] : false;
        if ($get) {
            return self::$_instance->get($get);
        }

        return self::$_instance;
    }

    /**
     * Возвращает конфигурационный массив значений по умолчанию
     * @return array
     */
    public function getDefault()
    {
        if (is_null($this->configDefault)) {
            $fileConfigDefault = $this->app()->get('appPath') . '/' . $this->app()->get('dirConfigs') . '/default.readonly.php';
            if (file_exists($fileConfigDefault)) {
                $this->configDefault = include_once($fileConfigDefault);
            } else {
                throw new Exception('File_exists "' . $fileConfigDefault . '" not found!');
            }
        }
        return $this->configDefault;
    }

    /**
     * Получает значение из конфигурационного файла
     * @param string $name
     * @return string
     */
    private function arraySearch($arr, $name)
    {
        $result = null;
        foreach ((array)$arr as $key => $ar)
        {
            if ($key == $name) {
                $result = $ar;
                break;
            }
        }
        return $result;
    }
    private function get($name)
    {
        $names = explode('.', $name);
        $result = $this->app()->get('configCustom');
        foreach ($names as $name) {
            $result = $this->arraySearch($result, $name);
        }
        if (is_null($result)) {
            $result = $this->getDefault();
            foreach ($names as $name) {
                $result = $this->arraySearch($result, $name);
            }
        }
        return $result;
    }
}