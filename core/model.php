<?php

namespace Core;

class Model
{
    const REL_ONE = 1;
    const REL_ALL = 2;

    protected $app = null;

    protected $_tableName = null;

    private $_rules = [];
    private $_relations = [];
    private $_with = [];
    private $_oldAttributes;
    private $_attributes;

    public $isNew = true;
    public $validateErrors = [];

    /**
     * Model constructor.
     */
    public function __construct()
    {
        if ($this->isNew) {
            $this->app = appDeneve();
        }
    }

    public function inic() { }

    protected function rules() {return true;}

    protected function relations() {return [];}

    /**
     * Возвращает имя таблицы
     * @return null|string
     */
    public function getTableName()
    {
        if (is_null($this->_tableName)) {
            $name = explode('\\', get_class($this));
            $name = end($name);
            $this->_tableName = strtolower(str_replace('Model', '', $name));
        }
        return $this->_tableName;
    }

    public function setTableName($name)
    {
        $this->_tableName = $name;
    }

    public function setRules($rules)
    {
        $this->_rules = $rules;
    }

    public function setRelations($relations)
    {
        $this->_relations = $relations;
    }

    /**
     * Клонирует модель и заполняет атрибутами
     * @param $table
     * @param $attributes
     * @return Model
     */
    private function cloneModel($table, $attributes)
    {
        $model = new Model;
        $model->isNew = false;
        $model->setTableName($table);
        $model->setRules($this->rules());
        $model->setRelations($this->relations());
        $model->setAttributes($attributes);
        return $model;
    }

    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Валидация атрибутов
     * @param string $scenario
     * @return bool
     */
    public function validate($scenario = 'default')
    {
        if (isset($this->_rules[$scenario])) {
            foreach ($this->_rules[$scenario] as $fields => $validates) {
                $fieldsEx = explode(',', $fields);
                $validatesEx = explode(',', $validates);
                foreach ($fieldsEx as $field) {
                    $field = trim($field);
                    foreach ($validatesEx as $validate) {
                        $er = false;
                        if (!$er) {
                            if (preg_match('/min(.*?)\)/iu', $validates, $match)) {
                                $val = (int)str_replace('(', '', $match[1]);
                                if (!$this->_validate_min($field, $val)) {
                                    $er = true;
                                }
                            }
                        }
                        if (!$er) {
                            if (preg_match('/max(.*?)\)/iu', $validates, $match)) {
                                $val = (int)str_replace('(', '', $match[1]);
                                if (!$this->_validate_max($field, $val)) {
                                    $er = true;
                                }
                            }
                        }
                        if (!$er) {
                            $method = '_validate_' . $validate;
                            if (method_exists($this, $method)) {
                                if (!$this->{$method}($field)) {
                                    $er = true;
                                }
                            }
                        }
                        if ($er) {
                            if (preg_match('/errorText(.*?)\)/iu', $validates, $match)) {
                                $val = str_replace('(', '', $match[1]);
                            } else {
                                $val = $field . ': error';
                            }
                            $this->validateErrors[$field] = $val;
                            break;
                        }
                    }
                }
            }
        }
        return !count($this->validateErrors) ? true : false;
    }

    private function _validate_min($field, $val)
    {
        $result = false;
        if (isset($this->_attributes[$field])) {
            return $val <= mb_strlen($this->_attributes[$field], 'utf-8');
        }
        return $result;
    }

    private function _validate_max($field, $val)
    {
        $result = false;
        if (isset($this->_attributes[$field])) {
            return mb_strlen($this->_attributes[$field], 'utf-8') <= $val;
        }
        return $result;
    }

    private function _validate_numeric($field)
    {
        $result = false;
        if (isset($this->_attributes[$field])) {
            $result = is_numeric($this->_attributes[$field]);
        }
        return $result;
    }

    /**
     * Получение реляционного объекта/объектов
     * @param $name
     * @param $arguments
     * @return null|Model|Model's
     */
    public function __call($name, $arguments)
    {
        if (in_array($name, $this->_with)) {
            return $this->_with[$name];
        }
        foreach ($this->_relations as $key => $relation) {
            if ($name == $key) {
                $relationKey = key($relation);
                $rel = explode('(', str_replace(')', '', $relation[$relationKey]));
                $relTable = trim($rel[0]);
                if (isset($rel[1]) && !empty(trim($rel[1]))) {
                    $t = explode(',', $rel[1]);
                    if (count($t) == 1) {
                        $curField = 'id';
                        $relField = trim($t[0]);
                    } else {
                        $curField = trim($t[0]);
                        $relField = isset($t[1]) ? trim($t[1]) : $curField;
                    }
                } else {
                    $curField = $relField = 'id';
                }
                $rel = null;
                if ($relationKey == self::REL_ONE) {
                    if ($result = appDeneve()->db($relTable)->findOne($relField . ' = ?', $this->_attributes[$curField])) {
                        $rel = $this->cloneModel($relTable, $result);
                    }
                } else {
                    $rel = [];
                    $results = appDeneve()->db($relTable)->find($relField . ' = ?', $this->_attributes[$curField]);
                    foreach ($results as $result) {
                        $model = $this->cloneModel($relTable, $result);
                        $this->_with[$name] = $model;
                        $rel[] = $model;
                    }
                }
                return $rel;
                break;
            }
        }
        return null;
    }

    /**
     * Получение атрибута
     * @param $name
     * @return null|string
     */
    public function __get($name)
    {
        return isset($this->_attributes[$name]) ? $this->_attributes[$name] : null;
    }

    /**
     * Установка атрибута
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if ($name == 'setAttributes') {
            $this->setAttributes($value);
        } else {
            $this->_attributes[$name] = $value;
        }
    }

    /**
     * Массовое присваивание атрибутов
     * @param $values
     */
    public function setAttributes($values)
    {
        if (is_array($values)) {
            $this->_oldAttributes = $this->_attributes = $values;
        }
    }

    /**
     * Поиск и возврат одной модели
     * @param string $prepare
     * @param array $values
     * @param null|array $params
     * @return bool|Model
     */
    public function findOne($prepare = '', $values = [], $params = null)
    {
        if ($result = appDeneve()->db($this->getTableName())->findOne($prepare, $values, $params)) {
            return $this->cloneModel($this->getTableName(), $result);
        }
        return false;
    }

    /**
     * Поиск моделей по критерию
     * @param string $prepare
     * @param array $values
     * @param null|array $params
     * @return array Model's
     */
    public function find($prepare = '', $values = [], $params = null)
    {
        $return = [];
        $results = appDeneve()->db($this->getTableName())->find($prepare, $values, $params);
        foreach ($results as $result) {
            $return[] = $this->cloneModel($this->getTableName(), $result);
        }
        return $return;
    }

    /**
     * Сохранение изменений в модели
     * @return bool
     */
    public function save()
    {
        if (isset($this->_oldAttributes['id'])) {
            $updates = '';
            $values = [];
            foreach ($this->_oldAttributes as $key => $oldAttribute) {
                $attribute = $this->_attributes[$key];
                if ($oldAttribute === $attribute) {
                    continue;
                }
                $updates.= ', t.' . $key . ' = ?';
                $values[] = $attribute;
                $this->_oldAttributes[$key] = $attribute;
            }
            if ($updates) {
                return appDeneve()->db($this->getTableName())->update($this->_oldAttributes['id'], ltrim($updates, ','), $values);
            }
        }
        return false;
    }

    /**
     * Удаление модели из таблицы
     * @return bool
     */
    public function delete()
    {
        if (isset($this->_oldAttributes['id'])) {
            return appDeneve()->db($this->getTableName())->delete($this->_oldAttributes['id'], [], ['limit' => 1]);

        }
        return false;
    }
}