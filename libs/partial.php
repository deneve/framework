<?php
/*
 * Библиотека для работы с подключаемыми блоками
 */
class Partial extends \Core\Lib
{
    private static $_instance = null;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        if (!isset($arguments[0])) {
            return self::$_instance;
        }

        return self::$_instance->render($arguments);
    }

    /**
     * Подключает html блок в слой; такие partial's должны находиться в каталоге layouts/partials
     * @param $name
     * @param array $params
     * @return null|string
     * @throws Exception
     */
    public function layout($name, $params = [])
    {
        $filename = $this->app()->get('appPath') . '/' . $this->app()->get('dirLayouts') . '/partials/' . strtolower($name) . '.php';
        if (is_file($filename)) {
            $params = array_merge((array)$params, ['_' => $this->app()->translate(), 'app' => $this->app()]);
            ob_start();
            extract($params);
            include $filename;
            return ob_get_clean();
        } else {
            throw new Exception('File_exists "' . $filename . '" not found!');
        }
        return null;
    }

    /**
     * Подключает html блок; partial's должны находиться в каталоге views/partials модуля
     * @return null|string
     */
    private function render($arguments)
    {
        if ((isset($arguments[0]) && is_string($arguments[0])) && (isset($arguments[1]) && is_string($arguments[1]))) {
            $module = $arguments[0];
            $partial = $arguments[1];
            $params = isset($arguments[2]) ? (array)$arguments[2] : [];
        } elseif (isset($arguments[0]) && is_string($arguments[0])) {
            $module = $this->app()->get('module');
            $partial = $arguments[0];
            $params = isset($arguments[1]) ? (array)$arguments[1] : [];
        }
        $params = array_merge($params, ['app' => $this->app()]);
        $filename = $this->app()->get('appPath') . '/' . $this->app()->get('dirModules') . '/' . $module . '/views/partials/' . strtolower($partial) . '.php';
        if (is_file($filename)) {
            ob_start();
            extract($params);
            include $filename;
            return ob_get_clean();
        }
        return null;
    }
}