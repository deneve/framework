<?php
/*
 * Deneve PHP framework
 * author: Sergeev Dmitry, email: ragnhild.iceland@gmail.com, skype: mrwolf127
 * https://bitbucket.org/deneve/framework
 */

require 'core/_once.php';
require 'core/module.php';
require 'core/controller.php';
require 'core/model.php';
require 'core/lib.php';

class frameworkDeneveData
{
    private static $_instance = null;

    private $version = '1.0.372a';

    private $dirLibs = 'libs';
    private $dirConfigs = 'configs';
    private $dirLayouts = 'layouts';
    private $dirModules = 'modules';

    public $appPath;
    public $basePath;
    public $debugMode;
    public $configCustom;

    public $factories = [];

    public $module, $controller, $action, $params;

    static public function ins()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function get($name)
    {
        return property_exists($this, $name) ? $this->{$name} : false;
    }
}

class frameworkDeneve
{
    /**
     * frameworkDeneve constructor.
     * @param string $appPath
     * @param string $basePath
     * @param int $debugMode
     * @param string $configName
     */
    public function __construct($appPath, $basePath = '/', $debugMode = 1, $configName = 'main')
    {
        global $appDeneve;
        frameworkDeneveData::ins()->appPath = $appPath;
        frameworkDeneveData::ins()->basePath = $basePath;
        frameworkDeneveData::ins()->debugMode = $debugMode;
        error_reporting(($debugMode && $debugMode < 3) ? E_ALL & ~E_NOTICE : 0);
        frameworkDeneveData::ins()->configCustom = include_once($appPath . '/' . frameworkDeneveData::ins()->get('dirConfigs') . '/' . $configName . '.php');
        $appDeneve = $this;
    }

    /**
     * Запуск фреймворка
     * @param array $route
     * @param bool $obs
     */
    public function run($route = null, $obs = true)
    {
        try {
            $route = is_null($route) ? $this->getRoute() : $route;
            frameworkDeneveData::ins()->module = $route['module'];
            frameworkDeneveData::ins()->controller = $route['controller'];
            frameworkDeneveData::ins()->action = $route['action'];
            frameworkDeneveData::ins()->params = $route['params'];
            $modulePath = frameworkDeneveData::ins()->get('appPath') . '/' . frameworkDeneveData::ins()->get('dirModules') . '/' . frameworkDeneveData::ins()->get('module') . '/index.php';
            if (file_exists($modulePath)) {
                include_once $modulePath;
                $moduleName = frameworkDeneveData::ins()->get('module') . 'Module';
                $moduleClass = new $moduleName;
                $resultModule = $moduleClass->runModule(
                    frameworkDeneveData::ins()->get('appPath') . '/' . frameworkDeneveData::ins()->get('dirModules') . '/' . frameworkDeneveData::ins()->get('module') . '/controllers/' . frameworkDeneveData::ins()->get('controller') . '.php'
                );
            } else {
                $resultModule = false;
            }
            if (!$resultModule && $obs) {
                $err404 = explode('/', (string)$this->config('default.error404'));
                $this->run([
                    'module' => isset($err404[0]) ? $err404[0] : '',
                    'controller' => isset($err404[1]) ? $err404[1] : '',
                    'action' => isset($err404[2]) ? $err404[2] : '',
                    'params' => ''
                ], false);
            }
        } catch (Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * Добавляет класс в фабрику
     * @param $name
     * @param bool|string $path
     * @return bool|mixed
     * @throws Exception
     */
    public function processFactory($name, $path, $module = false)
    {
        $object = false;
        foreach (frameworkDeneveData::ins()->get('factories') as $key => $factory) {
            if ($key == $path) {
                $object = $factory;
                break;
            }
        }
        if (!$object) {
            if (file_exists($path)) {
                include_once $path;
                $name = (!$module ? frameworkDeneveData::ins()->get('module') : $module) . '\\' . $name;
                frameworkDeneveData::ins()->factories[$path] = $object = new $name;
            } else {
                throw new Exception('File_exists "' . $path . '" not found!');
            }
        }
        return $object;
    }

    /**
     * Обработка вызовов
     * @param $name
     * @param null $arguments
     * @return null|mixed
     */
    private function call($name, $arguments = null)
    {
        try {
            global $factoriesDeneve;
            $nameClass = ucfirst($name);
            if (isset($factoriesDeneve[$nameClass])) {
                $class = $factoriesDeneve[$nameClass];
                return $class::getInstance($arguments);
            } else {
                $lib = __DIR__ . '/' . frameworkDeneveData::ins()->get('dirLibs') . '/' . $nameClass . '.php';
                if (file_exists($lib)) {
                    include_once $lib;
                    $factoriesDeneve[$nameClass] = $nameClass;
                    return $nameClass::getInstance($arguments);
                }
            }
        } catch (Exception $e) {
            $this->handleError($e);
        }
        return null;
    }

    public function __call($name, $arguments)
    {
        return $this->call($name, $arguments);
    }

    public function __get($name)
    {
        return $this->call($name);
    }

    /**
     * Возвращает свойство или call вызов
     * @param $var
     * @param null $default
     * @return mixed|null
     */
    public function get($var, $default = null)
    {
        $result = frameworkDeneveData::ins()->get($var);
        return $result ? $result : $default;
    }

    /*
     * "" или "/"                               => defaultModule/defaultController/defaultAction
     * "/myAction"                              => defaultModule/defaultController/myAction
     * "/otherController/myAction"              => defaultModule/otherController/myAction
     * "/otherModule/otherController/myAction"  => otherModule/otherController/myAction
     *
     * @param bool|string $myroute
     * @return array
     */
    public function getRoute($myroute = false)
    {
        $cdm = explode('/', (string)$this->config('default.module'));
        $defaultModule = isset($cdm[0]) ? $cdm[0] : '';
        $defaultController = isset($cdm[1]) ? $cdm[1] : '';
        $defaultAction = isset($cdm[2]) ? $cdm[2] : '';
        if ($myroute) {
            $r = $myroute;
        } else {
            if (isset($_GET['route'])) {
                $r = $_GET['route'];
            } else {
                $r = $_SERVER['REQUEST_URI'];
            }
        }
        $parts = explode('?', $r, 2);
        $r = explode('/', strtolower(ltrim($parts[0], '/')), 4);
        if ((!isset($r[2]) || empty($r[2])) && (!isset($r[1]) || empty($r[1]))) {
            $r[3] = '';
            $r[2] = isset($r[0]) && !empty($r[0]) ? $r[0] : $defaultAction;
            $r[1] = $defaultController;
            $r[0] = $defaultModule;
        } elseif (!isset($r[2]) || empty($r[2])) {
            $r[3] = '';
            $r[2] = isset($r[1]) && !empty($r[1]) ? $r[1] : $defaultAction;
            $r[1] = isset($r[0]) && !empty($r[0]) ? $r[0] : $defaultController;
            $r[0] = $defaultModule;
        } else {
            $r[3] = isset($r[3]) ? $r[3] : '';
            $r[2] = isset($r[2]) && !empty($r[2]) ? $r[2] : $defaultAction;
            $r[1] = isset($r[1]) && !empty($r[1]) ? $r[1] : $defaultController;
            $r[0] = isset($r[0]) && !empty($r[0]) ? $r[0] : $defaultModule;
        }
        return [
            'module' => $r[0],
            'controller' => $r[1],
            'action' => $r[2],
            'params' => $r[3]
        ];
    }

    /**
     * Получает базовую директорию приложения
     * @return string
     */
    public function basePath()
    {
        return frameworkDeneveData::ins()->get('basePath');
    }

    /**
     * Формирует ссылки с учетом текущего модуля, языка сайта и параметров
     * @param string $path
     * @param array $params
     * @return string
     */
    public function u($path = '', $params = [])
    {
        if ($path) {
            $r = explode('/', $path, 4);
            $cdm = explode('/', (string)$this->config('default.module'));
            $defaultModule = isset($cdm[0]) ? $cdm[0] : '';
            $defaultController = isset($cdm[1]) ? $cdm[1] : '';
            $rgen = [];
            if ((!isset($r[2]) || empty($r[2])) && (!isset($r[1]) || empty($r[1]))) {
                $rgen[2] = isset($r[0]) && !empty($r[0]) ? $r[0] : frameworkDeneveData::ins()->get('action');
                if (frameworkDeneveData::ins()->get('module') != $defaultModule || frameworkDeneveData::ins()->get('controller') != $defaultController) {
                    $rgen[1] = frameworkDeneveData::ins()->get('controller');
                }
                if (frameworkDeneveData::ins()->get('module') != $defaultModule) {
                    $rgen[0] = frameworkDeneveData::ins()->get('module');
                }
            } elseif (!isset($r[2]) || empty($r[2])) {
                $rgen[2] = isset($r[1]) && !empty($r[1]) ? $r[1] : frameworkDeneveData::ins()->get('action');
                $rgen[1] = isset($r[0]) && !empty($r[0]) ? $r[0] : frameworkDeneveData::ins()->get('controller');
                if (frameworkDeneveData::ins()->get('module') != $defaultModule) {
                    $rgen[0] = frameworkDeneveData::ins()->get('module');
                }
            } else {
                if (isset($r[3]) && !empty($r[3])) {
                    $rgen[3] = $r[3];
                }
                $rgen[2] = isset($r[2]) && !empty($r[2]) ? $r[2] : frameworkDeneveData::ins()->get('action');
                $rgen[1] = isset($r[1]) && !empty($r[1]) ? $r[1] : frameworkDeneveData::ins()->get('controller');
                $rgen[0] = isset($r[0]) && !empty($r[0]) ? $r[0] : frameworkDeneveData::ins()->get('module');
            }
            ksort($rgen);
            $rgen = implode('/', $rgen);
        } else {
            $rgen = '';
        }

        $paramsPath = '';
        foreach ((array)$params as $key => $param) {
            $paramsPath.= '/' . $key . '/' . $param;
        }
        return frameworkDeneveData::ins()->get('basePath') . ($this->config('languageToUrl') ? $this->config('language') . '/' : '')  . $rgen . $paramsPath;
    }

    /**
     * Доступ к статическим ресурсам (images, js, css, etc)
     * @param string $path
     * @return string
     */
    public function s($path = '')
    {
        return frameworkDeneveData::ins()->get('basePath') . $this->config('default.static') . '/' . $path;
    }

    public function translate()
    {
        return function ($text = '') {
            return $text;
        };
    }

    /**
     * Обработка исключений throw's
     * debugMode (режим отладки; 0 = без ошибок, 1 = отладка, 2 = отладка+логирование, 3 = только логирование. default: 1)
     * @param exception $e
     */
    private function handleError($e)
    {
        $debugMode = frameworkDeneveData::ins()->get('debugMode');
        if ($debugMode && $debugMode < 3) {
            $filePath = $e->getFile();
            $fileLine = $e->getLine();
            echo '<p style="padding: 6px; color: #ff0000; border-bottom: 3px double #da0825;">Exception: <b>' , $e->getMessage() , '</b></p>';
            echo '<p style="padding: 6px; color: #000; background-color: #f1f1f1;">' , $filePath , ' #' , $fileLine , '</p>';
            if ($file = file($filePath)) {
                $text = '';
                $lineFrom = $fileLine - 4;
                $lineTo = $fileLine + 3;
                for ($i = $lineFrom; $i < $lineTo; $i++) {
                    $text.= isset($file[$i]) ? $file[$i] . PHP_EOL : '';
                }
                echo '<pre style="padding: 6px; font-size: 12px; color: #000; line-height: 0.8; background-color: #f9dbd6;">' , $text , '</pre>';
                echo '<small>frameworkDeneve v.' , frameworkDeneveData::ins()->get('version') , '</small>';
            }
            var_dump($e->getTrace());
        } else {
            echo 'Technical error, please try later ...' , PHP_EOL;
        }
        if ($debugMode >= 2) {
            // TODO: реализовать логирование ошибок
        }
    }
}