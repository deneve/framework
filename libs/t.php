<?php
/*
 * Библиотека для работы с HTML тегами
 */
class T extends \Core\Lib
{
    private static $_instance = null;

    private $endTag;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        self::$_instance->endTag = isset($arguments[0]) ? (bool)$arguments[0] : true;

        return self::$_instance;
    }

    /**
     * Любой HTML tag
     * @param $name
     * @param null $arguments
     * @return string
     */
    public function __call($name, $arguments = null)
    {
        $contents = isset($arguments[0]) ? $arguments[0] : [];
        if (!is_array($contents)) {
            $contents = [$contents];
        }
        $params = isset($arguments[1]) ? (array)$arguments[1] : [];
        $level = 0;
        $v = current($params);
        while (is_array($v)) {
            $level++;
            $v = current($v);
        }
        if (!$level) {
            $params = [$params];
        }

        $return = '';
        $forData = count($contents) > count($params) ? $contents : $params;
        foreach ($forData as $key => $data) {
            $addParams = isset($params[$key]) ? $this->addParams($params[$key]) : '';
            $addContent = count($contents) == 1 ? $contents[0] : (isset($contents[$key]) ? $contents[$key] : '');
            $return.= '<' . $name . $addParams . '>' . $addContent . ($this->endTag ? '</' . $name . '>' : '');
        }
        return $return;
    }

    // =================================================================================================================
    private function addParams($params)
    {
        $addParams = '';
        foreach ((array)$params as $key => $param) {
            $addParams.= ' ' . $key . '="' . $param . '"';
        }
        return $addParams;
    }
}