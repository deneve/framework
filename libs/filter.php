<?php
/*
 * Библиотека для работы с фильтрами
 */
class Filter extends \Core\Lib
{
    private static $_instance = null;

    private $filters;
    private $anon;

    static public function getInstance($arguments)
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        $vars = $arguments[0];
        self::$_instance->filters = isset($arguments[1]) ? $arguments[1] : false;
        self::$_instance->anon = isset($arguments[2]) ? $arguments[2] : false;
        return self::$_instance->handle($vars);
    }

    public function handle($vars)
    {
        if (!is_array($vars)) {
            $array = false;
            $vars = [$vars];
        } else {
            $array = true;
        }
        if (self::$_instance->filters || self::$_instance->anon) {
            $filters = explode(',', self::$_instance->filters);
            foreach ($vars as &$var) {
                foreach ($filters as $filter) {
                    $filter = trim($filter);
                    if (method_exists($this, $filter)) {
                        $var = $this->{$filter}($var);
                    }
                }
                if (self::$_instance->anon) {
                    $var = self::$_instance->anon;
                }
            }
        }
        return !$array ? $vars[0] : $vars;
    }

    /**
     * @param $var
     * @return int
     */
    private function int($var)
    {
        return (int)$var;
    }

    private function float($var)
    {
        return (float)$var;
    }

    private function string($var)
    {
        return (string)$var;
    }

    private function trim($var)
    {
        return trim($var);
    }

    private function strip_tags($var)
    {
        return strip_tags($var);
    }
}