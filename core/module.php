<?php

namespace Core;

class Module
{
    protected $app;

    public function runModule($controllerPath)
    {
        $this->app = appDeneve();

        $this->inic();

        $resultModule = false;
        if ($controllerClass = $this->app->processFactory($this->app->get('controller') . 'Controller', $controllerPath)) {
            $controllerClass->inic();
            $actionName = $this->app->get('action') . 'Action';
            if (method_exists($controllerClass, $actionName)) {
                $resultModule = true;
                echo $controllerClass->{$actionName}($this->app->get('params'));
            }
        }
        return $resultModule;
    }

    public function inic() { }
}