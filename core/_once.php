<?php

if (isset($_GET['deneve'])) {
    die(json_encode([
        'type' => 'framework',
        'language' => 'PHP',
        'name' => 'Deneve'
    ]));
}

$appDeneve = null;

trait appDeneve
{

}

function appDeneve()
{
    global $appDeneve;
    return $appDeneve;
}