<?php

/*
 * Template Lib class
 */
class Template extends \Core\Lib
{
    private static $_instance = null;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function get()
    {
        return 'Template';
    }
}