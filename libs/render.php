<?php
/*
 * Библиотека для работы с слоями и видами
 */
class Render extends \Core\Lib
{
    private static $_instance = null;

    private $layout;
    private $params;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        $layout = isset($arguments[0]) ? $arguments[0] : false;
        if (is_array($layout)) {
            self::$_instance->layout = appDeneve()->get('module');
            self::$_instance->params = $layout;
        } else {
            self::$_instance->layout = $layout ? (string)$layout : appDeneve()->get('module');
            self::$_instance->params = isset($arguments[1]) ? $arguments[1] : false;
        }

        return self::$_instance;
    }

    private function getIncludeContents($filename, $params)
    {
        if (is_file($filename)) {
            ob_start();
            extract($params);
            include $filename;
            return ob_get_clean();
        } else {
            throw new Exception('File_exists "' . $filename . '" not found!');
        }
    }

    public function view($name = false, $params = [])
    {
        if ($name) {
            $vies_params = array_merge($params, ['_' => $this->app()->translate(), 'app' => $this->app()]);
            $view = $this->getIncludeContents(
                $this->app()->get('appPath') . '/' . $this->app()->get('dirModules') . '/' . $this->app()->get('module') . '/views/' . strtolower($name) . '.php',
                $vies_params
            );
        } else {
            $view = null;
        }

        $currentController = $this->app()->processFactory(
            $this->app()->get('controller') . 'Controller',
            $this->app()->get('appPath') . '/' . $this->app()->get('dirModules') . '/' . $this->app()->get('module') . '/controllers/' . $this->app()->get('controller') . '.php'
        );
        $title = !empty($currentController->titlePage) ? $currentController->titlePage . ' ' . $currentController->sep . ' ' : '';
        $title.= !empty($currentController->title) ? $currentController->title : $this->app()->config('site.name');

        $return = [
            '_' => $this->app()->translate(),
            'app' => $this->app(),
            'title' => $title,
            'content' => $view
        ];

        if (self::$_instance->params) {
            $return = array_merge($return, self::$_instance->params);
        }
        extract($return);
        include $this->app()->get('appPath') . '/' . $this->app()->get('dirLayouts') . '/' . strtolower($this->layout) . '.php';
    }
}