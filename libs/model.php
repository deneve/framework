<?php
/*
 * Библиотека для работы с моделями
 */
class Model extends \Core\Lib
{
    private static $_instance = null;

    static public function getInstance($arguments)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return isset($arguments[0]) ? self::$_instance->get((string)$arguments[0]) : false;
    }

    private function get($name)
    {
        $name = explode('/', $name, 2);
        if (!isset($name[1])) {
            $pModule = $this->app()->get('module');
            $pModel = trim($name[0]);
        } else {
            $pModule = trim($name[0]);
            $pModel = trim($name[1]);
        }
        $modelName = $pModel . 'Model';
        $modelPath = $this->app()->get('appPath') . '/' . $this->app()->get('dirModules') . '/' . $pModule . '/models/' . $pModel . '.php';
        if ($model = $this->app()->processFactory($modelName, $modelPath, $pModule)) {
            $model->inic();
        }
        return $model;
    }
}