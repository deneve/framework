<?php
/*
 * Библиотека для работы с деревом Dom Html
 */
class Dom extends \Core\Lib
{
    private static $_instance = null;

    private $dom;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function load($html)
    {
        $this->dom = new DOMDocument();
        $this->dom->LoadHtml(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        return $this;
    }

    public function getDom()
    {
        return $this->dom;
    }

    public function findById($id)
    {
        return $this->dom->getElementById($id);
    }

    public function findOneByTag($tag, $attribute = false, $value = false)
    {
        return $this->findByTag($tag, $attribute, $value, true);
    }

    public function findByTag($tag, $attribute = false, $value = false, $one = false)
    {
        $elems = [];
        foreach ($this->dom->getElementsByTagName($tag) as $elem) {
            if ((!$attribute || $elem->hasAttribute($attribute)) && (!$value || $elem->getAttribute($attribute) == $value)) {
                if ($one) {
                    return $elem;
                } else {
                    $elems[] = $elem;
                }
            }
        }
        return $elems;
    }

    public function removeAttribute($tag, $attribute, $value = false)
    {
        foreach ($this->dom->getElementsByTagName($tag) as $elem) {
            if ($elem->hasAttribute($attribute) && (!$value || $elem->getAttribute($attribute) == $value)) {
                $elem->removeAttribute($attribute);
            }
        }
    }

    public function save()
    {
        $html = $this->dom->saveHTML($this->dom->documentElement);
        $html = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $html);
        return $html;
    }

}